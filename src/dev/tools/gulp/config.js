/**
 * Default (shareable) frontools config.
 *
 * If you need to edit configs -> please use configExtend.js in current folder
 */

const path = require('path'),
  _ = require('lodash'),
  fs = require('fs'),
  PluginError = require('plugin-error');

module.exports = (magentoRoot) => {
  if (!magentoRoot) {
    throw new PluginError({
      plugin: 'Gulp Task Runner',
      message: 'No Magento root path is specified.',
      showStack: false
    });
  }
  
  let config = {
    tasks: {
      list: ['scss', 'generate'],
      default: ['scss']
    },
    autoprefixer: {
      browsers: ['> 1%', 'IE >= 10', 'iOS 7']
    },
    scss: {
      source: '/web/assets/scss',
      mask: '/**/*.scss',
      destination: '/web/assets/css',
      options: {
        outputStyle: process.env.NODE_ENV === 'development' ? 'expanded' : 'compressed',
      },
      linting: {
        config: path.resolve(magentoRoot, '.scss-lint.yml')
      }
    },
    js: {
      sources: [],
      destination: '/web/js'
    },
    watch: {
      scss: {
        source: '/web/assets/scss',
        mask: '/**/*.scss',
        options: { ignoreInitial: false }
      }
    }
  };
  
  if (!fs.existsSync(path.resolve(__dirname, './configExtend.js'))) {
    return config;
  }
  customConfig = require('./configExtend');
  
  return _.merge(config, customConfig);
};
