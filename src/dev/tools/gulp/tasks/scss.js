'use-strict';

const merge = require('merge-stream'),
  context = require('../context'),
  scssSources = require('./helper/build-scss-sources'),
  scssConstructor = require('./helper/scss-constructor');

module.exports = function () {
  let tasks = scssSources(context).map(config => {
    return scssConstructor(context, config);
  });
  
  return merge(tasks);
};
