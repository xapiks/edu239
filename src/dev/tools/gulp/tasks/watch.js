const watch = require('gulp-watch'),
  context = require('../context'),
  scssSources = require('./helper/build-scss-sources'),
  sccsConstructor = require('./helper/scss-constructor');

module.exports = () => {
  scssSources(context).forEach(config => {
    watch(config.source, context.config.watch.scss.options, () => {
      sccsConstructor(context, config, true);
    });
  });
};
