var config = {
    map: {
        '*': {
            'leaflet': 'Scandiweb_StoreMap/js/leaflet',
            'vue': 'Scandiweb_StoreMap/js/vue',
        },
    },
};
