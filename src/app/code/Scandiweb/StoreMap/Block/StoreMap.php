<?php
/**
 * @author Artjoms Travkovs <artjoms.travkovs@scandiweb.com>
 */

namespace Scandiweb\StoreMap\Block;


use Magento\Framework\View\Element\Template;
use Scandiweb\StoreFinder\Model\ResourceModel\Store\Collection;
use Scandiweb\StoreFinder\Model\ResourceModel\Store\CollectionFactory;
use Scandiweb\StoreMap\Helper\Data;

class StoreMap extends Template
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Data
     */
    private $helperData;

    /**
     * StoreMap constructor.
     *
     * @param Template\Context  $context
     * @param CollectionFactory $collectionFactory
     * @param Data              $helperData
     * @param array             $data
     */
    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        Data $helperData,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->helperData = $helperData;
    }

    /**
     * @return array
     */
    public function getAllStores(): array
    {
        /** @var Collection $storeCollection */
        $storeCollection = $this->collectionFactory->create();
        $collection = $storeCollection
            ->addFieldToFilter('is_active', 1)
            ->addOrder('position', Collection::SORT_ORDER_ASC)
            ->toArray();

        return $collection['items'] ?? [];
    }

    /**
     * @return string
     */
    public function getLeafletApiKey(): string
    {
        return $this->helperData->getGeneralConfig('leafletAPIKey');
    }

    /**
     * @return string
  0   */
    public function getGoogleApiKey(): string
    {
        return $this->helperData->getGeneralConfig('googleAPIKey');
    }
}
