<?php
/**
 * @author Artjoms Travkovs <artjoms.travkovs@scandiweb.com>
 */

namespace Scandiweb\StoreMap\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_STORE_LOCATOR = 'storelocator/';

    /**
     * @param string   $code
     * @param int|null $storeId
     *
     * @return mixed
     */
    public function getGeneralConfig(string $code, int $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_STORE_LOCATOR . 'general/' . $code, $storeId);
    }

    /**
     * @param string $field
     * @param null   $storeId
     *
     * @return mixed
     */
    public function getConfigValue(string $field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }
}