define([
    'Scandiweb_StoreMap/js/leaflet',
    'Scandiweb_StoreMap/js/vue',
], function (L, Vue) {
    'use strict';

    return function (config) {
        var zoom = 15;
        var map = L.map('map').setView([57, 25], 7);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.light',
            accessToken: config.leafletApiKey,
        }).addTo(map);

        if (config.data.length <= 0) return;

        config.data.forEach(function (shop) {
            var marker = L.marker([shop.latitude, shop.longitude]).addTo(map);
            marker.bindPopup("<h4>" + shop.store_name + "</h4><span>Working Hours:&nbsp;" + shop.store_hours + "</span>");
        });

        function setLocationByIndex(index) {
            var store = config.data[index];
            map.setView([store.latitude, store.longitude], zoom);
        }

        function calculateDistanceBetweenPoints(lat1, lon1, lat2, lon2) {
            var φ1 = lat1 * Math.PI / 180, // latitude 1 in radians
                φ2 = lat2 * Math.PI / 180, // latitude 2 in radians
                Δλ = (lon2-lon1) * Math.PI / 180, // delta longitude in radians
                R = 6371e3; // Radius of Earth

            var distance = Math.acos(Math.sin(φ1) * Math.sin(φ2) + Math.cos(φ1) * Math.cos(φ2) * Math.cos(Δλ)) * R;

            return distance;
        }

        function getNearestShop(lat1, lon1) {
            var minIndex = 0;
            var distance = calculateDistanceBetweenPoints(lat1, lon1, config.data[0].latitude, config.data[0].longitude);

            for (var i = 1; i < config.data.length; i++) {
                if (distance > calculateDistanceBetweenPoints(lat1, lon1, config.data[i].latitude, config.data[i].longitude)) {
                    minIndex = i;
                }
            }

            return minIndex;
        }

        new Vue({
            el: '#shop-find',

            data: {
                shops: config.data,
            },

            methods: {
                selectShop: function (index) {
                    setLocationByIndex(index);
                },

                onPlaceChanged: function() {
                    var place = this.autocomplete.getPlace();
                    var lat = place.geometry.location.lat();
                    var lon = place.geometry.location.lng();

                    this.selectShop(getNearestShop(lat, lon));
                    this.$refs.autocomplete.value = '';
                },
            },

            mounted: function() {
                this.autocomplete = new google.maps.places.Autocomplete(
                    (this.$refs.autocomplete)
                );

                this.autocomplete.addListener('place_changed', this.onPlaceChanged);
            },
        });
    };

});