<?php
/**
 * @package Scandiweb_CmsPage
 */

namespace Scandiweb\CmsPage\Setup\Patch\Data;

use Magento\Framework\Module\Dir\Reader;
use \Magento\Framework\Setup\Patch\DataPatchInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Cms\Model\PageFactory;
use \Magento\Cms\Model\ResourceModel\Page;

class AddNewCmsPage implements DataPatchInterface {

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var Page
     */
    private $page;
    /**
     * @var Reader
     */
    private $directoryList;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        PageFactory $pageFactory,
        Page $page,
        Reader $directoryList
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->pageFactory = $pageFactory;
        $this->page = $page;
        $this->directoryList = $directoryList;
    }

    /**
     * {@inheritDoc}
     */
    public function apply()
    {
        $content = file_get_contents($this->directoryList->getModuleDir('', 'Scandiweb_CmsPage').'/Pages/second-assignment.html');

        $pageData = [
            'title' => 'Second assignment',
            'page_layout' => '1column',
            'meta_keywords' => '',
            'meta_description' => '',
            'identifier' => 'second-assignment',
            'content_heading' => '',
            'content' => $content,
            'layout_update_xml' => '',
            'url_key' => 'second-assignment',
            'is_active' => 1,
            'stores' => [0],
            'sort_order' => 0
        ];

        $this->moduleDataSetup->getConnection()->startSetup();
        $page = $this->pageFactory->create()->setData($pageData);

        $this->page->save($page);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritDoc}
     */
    public static function getDependencies()
    {
        return [];
    }


    /**
     * {@inheritDoc}
     */
    public function getAliases()
    {
        return [];
    }
}
