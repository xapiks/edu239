<?php

namespace Scandiweb\BadgeExtension\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Scandiweb\BadgeExtension\Model\Badge;

class BadgeActions extends Column
{

    const URL_PATH_EDIT = 'badges/badges/edit';
    const URL_PATH_DELETE = 'badges/badges/delete';

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * BadgeActions constructor.
     *
     * @param UrlInterface       $url
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        UrlInterface $url,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->url = $url;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item[Badge::COLUMN_BADGE_ID])) {
                    $item[$this->getData('name')] = [
                        'edit'   => [
                            'href'  => $this->url->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    Badge::COLUMN_BADGE_ID => $item[Badge::COLUMN_BADGE_ID],
                                ]
                            ),
                            'label' => __('Edit'),
                        ],
                        'delete' => [
                            'href'    => $this->url->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    Badge::COLUMN_BADGE_ID => $item[Badge::COLUMN_BADGE_ID],
                                ]
                            ),
                            'label'   => __('Delete'),
                            'confirm' => [
                                'title'   => __('Delete "${$.$data.name}"'),
                                'message' => __('Are you sure you want to delete badge "${$.$data.name}"?'),
                            ],
                        ],
                    ];
                }
            }
        }

        return $dataSource;
    }

}