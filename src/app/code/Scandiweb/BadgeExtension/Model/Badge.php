<?php

namespace Scandiweb\BadgeExtension\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Badge extends AbstractModel implements IdentityInterface
{
    const TABLE_NAME = 'scandiweb_badgeextension_badge';

    const COLUMN_BADGE_ID = 'badge_id';

    const COLUMN_NAME = 'name';
    const COLUMN_IMAGE = 'image';
    const COLUMN_STATUS = 'status';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';

    const CACHE_TAG = 'scandiweb_badgeextension_badge';

    protected $_cacheTag = 'scandiweb_badgeextension_badge';

    protected $_eventPrefix = 'scandiweb_badgeextension_badge';

    protected function _construct()
    {
        $this->_init('Scandiweb\BadgeExtension\Model\ResourceModel\Badge');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues(): array
    {
        return [];
    }
}
