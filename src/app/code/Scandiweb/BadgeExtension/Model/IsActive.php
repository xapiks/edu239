<?php

namespace Scandiweb\BadgeExtension\Model;

use Magento\Framework\Data\OptionSourceInterface;

class IsActive implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            [
                'label' => __('Active'),
                'value' => 1,
            ],
            [
                'label' => __('Disabled'),
                'value' => 0,
            ],
        ];
    }
}