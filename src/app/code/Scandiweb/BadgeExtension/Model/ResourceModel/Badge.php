<?php

namespace Scandiweb\BadgeExtension\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Scandiweb\BadgeExtension\Model\Badge as BadgeModel;

class Badge extends AbstractDb
{
    /**
     * Badge constructor.
     *
     * @param Context $context
     * @param null    $connectionName
     */
    public function __construct(Context $context, $connectionName = null)
    {
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init(BadgeModel::TABLE_NAME, BadgeModel::COLUMN_BADGE_ID);
    }
}
