<?php


namespace Scandiweb\BadgeExtension\Model\ResourceModel\Badge;


use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Scandiweb\BadgeExtension\Model\Badge;

class DataProvider extends AbstractDataProvider
{
    /** @var array */
    protected $loadedData;

    /** @var Collection */
    protected $collection;
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * @var Serialize
     */
    private $serializer;

    /**
     * DataProvider constructor.
     *
     * @param string                 $name
     * @param string                 $primaryFieldName
     * @param string                 $requestFieldName
     * @param CollectionFactory      $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param Serialize              $serializer
     * @param array                  $meta
     * @param array                  $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        Serialize $serializer,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->serializer = $serializer;
    }


    /**
     * @return array
     */
    public function getData(): array
    {
        if (!isset($this->loadedData)) {
            $this->loadedData = [];

            /** @var Badge $item */
            foreach ($this->collection->getItems() as $item) {
                $this->loadedData[$item->getId()] = $item->getData();
            }
        }

        $data = $this->dataPersistor->get('badge_store');

        if (!empty($data)) {
            /** @var Badge $badge */
            $badge = $this->collection->getNewEmptyItem();
            $badge->setData($data);

            $badgeData = $badge->getData();
            $badgeData['image'] = [$this->getImage($badgeData['image'])];

            $this->loadedData[$badge->getId()] = ['badge' => $badgeData];
            $this->dataPersistor->clear('badge_store');
        }

        return $this->loadedData;
    }

    /**
     * @param $image
     *
     * @return array
     */
    private function getImage($image): array
    {
        $data = $this->serializer->unserialize($image);
        $data['file'] = $data['name'];

        return $data;
    }


}