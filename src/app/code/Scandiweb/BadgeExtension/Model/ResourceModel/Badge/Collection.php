<?php

namespace Scandiweb\BadgeExtension\Model\ResourceModel\Badge;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Serialize\SerializerInterface;
use Psr\Log\LoggerInterface;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'badge_id';
    protected $_eventPrefix = 'scandiweb_badgeextension_badge_collection';
    protected $_eventObject = 'badge_collection';

    protected $serializer;

    /**
     * Collection constructor.
     *
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface        $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface       $eventManager
     * @param SerializerInterface    $serializer
     * @param AdapterInterface|null  $connection
     * @param AbstractDb|null        $resource
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        SerializerInterface $serializer,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        $this->serializer = $serializer;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    protected function _construct()
    {
        $this->_init('Scandiweb\BadgeExtension\Model\Badge', 'Scandiweb\BadgeExtension\Model\ResourceModel\Badge');
    }
}
