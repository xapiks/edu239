<?php

namespace Scandiweb\BadgeExtension\Helper;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\UrlInterface;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

class ImageUploader
{
    const MEDIA_FOLDER = 'badges/';

    /**
     * @var UploaderFactory
     */
    private $fileUploader;
    /**
     * @var AdapterFactory
     */
    private $adapterFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Filesystem\Directory\WriteInterface
     */
    private $mediaDirectory;

    /**
     * ImageUploader constructor.
     *
     * @param Filesystem            $filesystem
     * @param UploaderFactory       $fileUploader
     * @param AdapterFactory        $adapterFactory
     * @param StoreManagerInterface $storeManager
     *
     * @throws FileSystemException
     */
    public function __construct(
        Filesystem $filesystem,
        UploaderFactory $fileUploader,
        AdapterFactory $adapterFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->fileUploader = $fileUploader;
        $this->adapterFactory = $adapterFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $file
     *
     * @return array|bool
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function uploadBadgeImage($file): ?array
    {
        if (!($file || $file['name'])) {
            return false;
        }

        $target = $this->mediaDirectory->getAbsolutePath(self::MEDIA_FOLDER);

        $uploader = $this->fileUploader->create([
            'fileId' => 'badge[image]',
        ]);
        $uploader->setAllowedExtensions(['jpg', 'png', 'jpeg', 'gif']);

        $imageAdapter = $this->adapterFactory->create();
        $uploader->addValidateCallback('badge_image_upload', $imageAdapter, 'validateUploadFile');

        $uploader->setAllowCreateFolders(true);
        $uploader->setAllowRenameFiles(true);

        $result = $uploader->save($target);
        $result['url'] = $this->getImageUrl($result['file']);

        return $result;
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function getImageUrl($file): string
    {
        /** @var Store $store */
        $store = $this->storeManager->getStore();

        $mediaUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        return $mediaUrl . '/' . self::MEDIA_FOLDER . $file;
    }
}