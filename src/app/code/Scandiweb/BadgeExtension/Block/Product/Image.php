<?php


namespace Scandiweb\BadgeExtension\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Image as ImageBlock;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Framework\View\Element\Template\Context;
use Scandiweb\BadgeExtension\Model\Badge;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\Collection;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\CollectionFactory;

class Image extends ImageBlock
{
    /**
     * @var CollectionFactory
     */
    private $badgeCollectionFactory;
    /**
     * @var Serialize
     */
    private $serialize;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Image constructor.
     *
     * @param Context                    $context
     * @param CollectionFactory          $badgeCollectionFactory
     * @param Serialize                  $serialize
     * @param ProductRepositoryInterface $productRepository
     * @param array                      $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $badgeCollectionFactory,
        Serialize $serialize,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->badgeCollectionFactory = $badgeCollectionFactory;
        $this->serialize = $serialize;
        $this->productRepository = $productRepository;
    }

    /**
     * @return Badge | null
     * @throws NoSuchEntityException
     *
     */
    public function getBadgeInstance(): ?Badge
    {
        $id = $this->getData()['product_id'];

        /** @var Product $product */
        $product = $this->productRepository->getById($id);
        $badgeId = $product->getData('badge');

        /** @var Collection $badgeCollection */
        $badgeCollection = $this->badgeCollectionFactory->create();
        /** @var Badge $badge */
        $badge = $badgeCollection->getItemById($badgeId);

        return $badge;
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isBadgeActive(): bool
    {
        $badge = $this->getBadgeInstance();

        return $badge && $badge->getDataByKey('status') == 1;
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getBadge(): string
    {

        if (!$this->isBadgeActive()) return '';

        $badge = $this->getBadgeInstance();
        $image = $this->serialize->unserialize($badge->getDataByKey('image'));

        return $image['url'];
    }
}