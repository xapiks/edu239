<?php


namespace Scandiweb\BadgeExtension\Block\Product;


use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product\Gallery\ImagesConfigFactoryInterface;
use Magento\Catalog\Model\Product\Image\UrlBuilder;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Framework\Stdlib\ArrayUtils;
use Scandiweb\BadgeExtension\Model\Badge;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\Collection;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\CollectionFactory;

class Gallery extends \Magento\Catalog\Block\Product\View\Gallery
{
    /**
     * @var CollectionFactory
     */
    private $badgeCollectionFactory;
    /**
     * @var Serialize
     */
    private $serialize;
    /**
     * @var ImagesConfigFactoryInterface
     */
    private $imagesConfigFactory;

    /**
     * Gallery constructor.
     *
     * @param Context                           $context
     * @param ArrayUtils                        $arrayUtils
     * @param EncoderInterface                  $jsonEncoder
     * @param array                             $data
     * @param CollectionFactory                 $badgeCollectionFactory
     * @param Serialize                         $serialize
     * @param ImagesConfigFactoryInterface|null $imagesConfigFactory
     * @param array                             $galleryImagesConfig
     * @param UrlBuilder|null                   $urlBuilder
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        EncoderInterface $jsonEncoder,
        array $data = [],
        CollectionFactory $badgeCollectionFactory,
        Serialize $serialize,
        ImagesConfigFactoryInterface $imagesConfigFactory = null,
        array $galleryImagesConfig = [],
        UrlBuilder $urlBuilder = null
    ) {
        parent::__construct($context, $arrayUtils, $jsonEncoder, $data, $imagesConfigFactory, $galleryImagesConfig, $urlBuilder);

        $this->badgeCollectionFactory = $badgeCollectionFactory;
        $this->serialize = $serialize;
        $this->imagesConfigFactory = $imagesConfigFactory;
    }

    /**
     * @return Badge | null
     */
    public function getBadgeInstance(): ?Badge
    {
        $product = $this->getProduct();

        $badgeId = $product->getData('badge');

        /** @var Collection $badgeCollection */
        $badgeCollection = $this->badgeCollectionFactory->create();
        /** @var Badge $badge */
        $badge = $badgeCollection->getItemById($badgeId);

        return $badge ?? null;
    }

    /**
     * @return bool
     */
    public function isBadgeActive(): bool
    {
        $badge = $this->getBadgeInstance();

        return $badge && $badge->getDataByKey('status') == 1;
    }

    /**
     * @return string
     */
    public function getBadge(): string
    {
        if (!$this->isBadgeActive()) return '';

        $badge = $this->getBadgeInstance();
        $image = $this->serialize->unserialize($badge->getDataByKey('image'));

        return $image['url'];
    }
}