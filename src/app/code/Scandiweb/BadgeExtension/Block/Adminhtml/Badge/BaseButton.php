<?php

namespace Scandiweb\BadgeExtension\Block\Adminhtml\Badge;


use Magento\Backend\Block\Widget\Context;
use Magento\Framework\UrlInterface;

class BaseButton
{
    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * BaseButton constructor.
     *
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
    }

    /**
     * @param string $route
     * @param array  $params
     *
     * @return string
     */
    public function getUrl(string $route = '', array $params = []): string
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}