<?php

namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Scandiweb\BadgeExtension\Helper\ImageUploader;

class Add extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;
    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * Constructor
     *
     * @param Context       $context
     * @param PageFactory   $resultPageFactory
     * @param JsonFactory   $resultJsonFactory
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->imageUploader = $imageUploader;
    }

    /**
     * @return ResponseInterface|Json|Page
     * @throws Exception
     */
    public function execute()
    {
        /** @var Http $request */
        $request = $this->getRequest();

        if ($request->isAjax() && ($request->getParam('param_name') == 'badge[image]')) {
            $file = $request->getFiles('badge')['image'];

            if ($result = $this->imageUploader->uploadBadgeImage($file)) {
                return $this->resultJsonFactory->create()->setData($result);
            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Add Badge'));


        return $resultPage;
    }
}
