<?php

namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Scandiweb\BadgeExtension\Helper\ImageUploader;
use Scandiweb\BadgeExtension\Model\Badge;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\Collection;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\CollectionFactory;

class Edit extends Action
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;
    /**
     * @var CollectionFactory
     */
    private $badgeCollectionFactory;
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var ImageUploader
     */
    private $imageUploader;
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * Constructor
     *
     * @param Context                $context
     * @param PageFactory            $resultPageFactory
     * @param JsonFactory            $resultJsonFactory
     * @param CollectionFactory      $badgeCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param Session                $session
     * @param ImageUploader          $imageUploader
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        CollectionFactory $badgeCollectionFactory,
        DataPersistorInterface $dataPersistor,
        Session $session,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->badgeCollectionFactory = $badgeCollectionFactory;
        $this->dataPersistor = $dataPersistor;
        $this->session = $session;
        $this->imageUploader = $imageUploader;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        /** @var Http $request */
        $request = $this->getRequest();

        if ($request->isAjax() && ($request->getParam('param_name') == 'badge[image]')) {
            $file = $request->getFiles('badge')['image'];

            if ($result = $this->imageUploader->uploadBadgeImage($file)) {
                return $this->resultJsonFactory->create()->setData($result);
            }
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Badge'));

        $id = (int)$this->getRequest()->getParam(Badge::COLUMN_BADGE_ID);

        /** @var Collection $badgeCollection */
        $badgeCollection = $this->badgeCollectionFactory->create();
        /** @var Badge $badge */
        $badge = $badgeCollection->getItemById($id);

        $data = $this->session->getFormData(true);
        if (!empty($data)) {
            $badge->setData($data);
        }

        $this->dataPersistor->set('badge_store', $badge->getData());

        if (!$badge) {
            return $this->_redirect('*/*/index');
        }

        return $resultPage;
    }
}