<?php


namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResourceConnection;
use Scandiweb\BadgeExtension\Model\Badge;

abstract class AbstractMassAction extends Action
{
    /** @var ResourceConnection */
    protected $resource;

    /**
     * AbstractMassAction constructor.
     *
     * @param Action\Context     $context
     * @param ResourceConnection $resource
     */
    public function __construct(
        Action\Context $context,
        ResourceConnection $resource
    ) {
        parent::__construct($context);
        $this->resource = $resource;
    }

    /**
     * @return array
     */
    protected function getDBData(): array
    {
        $selected = $this->getRequest()->getParam('selected');
        if (is_array($selected)) {
            $isOk = true;

            $table = $this->resource->getTableName(Badge::TABLE_NAME);
            $where = $this->generateWhereInClause($selected, Badge::COLUMN_BADGE_ID);

            $connection = $this->resource->getConnection();

            return [
                'isOk'       => $isOk,
                'table'      => $table,
                'where'      => $where,
                'connection' => $connection,
            ];
        }

        return [
            'selected' => $selected,
            'isOk'     => false,
        ];
    }

    /**
     * Generate Where In Clause.
     *
     * @param array  $ids
     * @param string $column
     *
     * @return array
     */
    protected function generateWhereInClause(array $ids, $column): array
    {
        $text = '`' . $column . '` IN (';
        $text .= array_reduce($ids, function () {
            return '?,';
        });
        $text = substr($text, 0, -1) . ')';

        return [$text => $ids];
    }
}