<?php


namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class MassDelete extends AbstractMassAction
{
    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $data = (object)$this->getDBData();

        if ($data->isOk) {
            $data->connection->delete($data->table, $data->where);

            $where = $this->generateWhereInClause($data->selected, 'value');
            $data->connection->delete('catalog_product_entity_int', $where);
        }

        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}