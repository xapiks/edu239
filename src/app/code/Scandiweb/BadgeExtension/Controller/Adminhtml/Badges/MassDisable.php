<?php


namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;


use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class MassDisable extends AbstractMassAction
{
    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $data = (object)$this->getDBData();

        if ($data->isOk) {
            $data->connection->update($data->table, ['status' => 0], $data->where);
        }

        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}