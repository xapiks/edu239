<?php

namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Scandiweb\BadgeExtension\Model\Badge;

class Delete extends Action
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Delete constructor.
     *
     * @param Action\Context     $context
     * @param ResourceConnection $resource
     */
    public function __construct(
        Action\Context $context,
        ResourceConnection $resource
    ) {
        parent::__construct($context);
        $this->resource = $resource;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $badgeId = $this->getRequest()->getParam('badge_id') ?? null;
        if ($badgeId !== null) {
            $table = $this->resource->getTableName(Badge::TABLE_NAME);
            $connection = $this->resource->getConnection();

            $connection->delete(
                $table,
                ['`' . Badge::COLUMN_BADGE_ID . '` = ?' => $badgeId]
            );

            $table = $this->resource->getTableName('catalog_product_entity_int');
            $connection->delete(
                $table,
                ['`value` = ?' => $badgeId]
            );
        }

        return $this->_redirect('*/*/index');
    }
}