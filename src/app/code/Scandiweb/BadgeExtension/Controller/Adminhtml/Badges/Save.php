<?php


namespace Scandiweb\BadgeExtension\Controller\Adminhtml\Badges;


use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Serialize\Serializer\Serialize;
use Scandiweb\BadgeExtension\Model\Badge;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge as BadgeResource;

class Save extends Action
{
    /**
     * @var Serialize
     */
    private $serializer;
    /**
     * @var BadgeResource
     */
    private $badgeResource;
    /**
     * @var Badge
     */
    private $badge;

    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param Serialize      $serializer
     * @param Badge          $badge
     * @param BadgeResource  $badgeResource
     */
    public function __construct(
        Action\Context $context,
        Serialize $serializer,
        Badge $badge,
        BadgeResource $badgeResource
    ) {
        parent::__construct($context);
        $this->serializer = $serializer;
        $this->badgeResource = $badgeResource;
        $this->badge = $badge;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws AlreadyExistsException
     * @throws Exception
     */
    public function execute()
    {
        /** @var Http $request */
        $request = $this->getRequest();

        $badgeData = $request->getParam('badge');
        if (is_array($badgeData)) {
            $badgeData['image'] = $this->serializer->serialize($badgeData['image'][0]);

            $this->badge->setData($badgeData);
            $this->badgeResource->save($this->badge);

        }

        return $this->_redirect('*/*/index');
    }
}