<?php
/**
 * @author Artjoms Travkovs <artjoms.travkovs@scandiweb.com>
 */

namespace Scandiweb\BadgeExtension\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Patch is mechanism, that allows to do atomic upgrade data changes
 */
class BadgeDataPatch implements
    DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory          $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply(): void
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'badge',
            [
                'group'                      => '',
                'label'                      => 'Badge',
                'is_html_allowed_on_front'   => false,
                'default'                    => '',
                'note'                       => '',
                'global'                     => Attribute::SCOPE_STORE,
                'visible'                    => true,
                'required'                   => false,
                'user_defined'               => false,
                'searchable'                 => false,
                'filterable'                 => false,
                'comparable'                 => false,
                'visible_on_front'           => false,
                'visible_in_advanced_search' => false,
                'unique'                     => false,
                'frontend_class'             => '',
                'used_in_product_listing'    => true,
                'input'                      => 'select',
                'type'                       => 'int',
                'source'                     => 'Scandiweb\BadgeExtension\Config\Source\BadgeSetup',
                'backend'                    => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
            ]
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }
}
