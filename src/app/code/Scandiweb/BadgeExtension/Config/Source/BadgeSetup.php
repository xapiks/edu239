<?php


namespace Scandiweb\BadgeExtension\Config\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Scandiweb\BadgeExtension\Model\Badge;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\Collection;
use Scandiweb\BadgeExtension\Model\ResourceModel\Badge\CollectionFactory;

class BadgeSetup extends AbstractSource
{
    /**
     * @var CollectionFactory
     */
    private $badgeCollectionFactory;

    /**
     * BadgeSetup constructor.
     *
     * @param CollectionFactory $badgeCollectionFactory
     */
    public function __construct(
        CollectionFactory $badgeCollectionFactory
    ) {
        $this->badgeCollectionFactory = $badgeCollectionFactory;
    }

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        if ($this->_options === null) {
            $this->_options = $this->getBadgeOptions();
        }

        return $this->_options;
    }

    /**
     * @return array
     */
    final public function toOptionArray(): array
    {
        return $this->getBadgeOptions();
    }

    /**
     * @return array
     */
    private function getBadgeOptions(): array
    {
        /** @var Collection $collection */
        $collection = $this->badgeCollectionFactory->create();

        $options = [
            ['value' => '', 'label' => ' '],
        ];
        /** @var Badge $badge */
        foreach ($collection as $badge) {
            $options[] = ['value' => $badge->getId(), 'label' => $badge->getName()];
        }

        return $options;
    }
}