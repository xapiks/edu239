define([
    'Magento_Ui/js/grid/columns/select',
],
    function (Column) {
        'use strict';

        return Column.extend({
            getStatusColor: function (row) {
                if (row.status == 1) {
                    return '#85b73c';
                }

                return '#f47b20';
            },
        });
    }
);
