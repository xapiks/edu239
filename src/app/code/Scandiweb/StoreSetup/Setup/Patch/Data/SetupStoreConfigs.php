<?php
/**
 * @package Scandiweb_SetupStore
 */

namespace Scandiweb\StoreSetup\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Theme\Model\Config;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;


class SetupStoreConfigs implements DataPatchInterface
{

    /**
     * @var Config
     */
    private $themeConfig;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var WriterInterface
     */
    private $writer;
    /**
     * @var Store
     */
    private $storeResource;
    /**
     * @var Group
     */
    private $groupResource;

    /**
     * SetupStore constructor.
     * @param CollectionFactory $collectionFactory
     * @param Config $themeConfig
     * @param Group $groupResource
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param StoreFactory $storeFactory
     * @param Store $storeResource
     * @param WriterInterface $writer
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Config $themeConfig,
        Group $groupResource,
        ModuleDataSetupInterface $moduleDataSetup,
        StoreFactory $storeFactory,
        Store $storeResource,
        WriterInterface $writer
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->themeConfig = $themeConfig;
        $this->groupResource = $groupResource;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeFactory = $storeFactory;
        $this->storeResource = $storeResource;
        $this->writer = $writer;
    }

    /**
     * {@inheritDoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }


    /**
     * {@inheritDoc}
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function apply(): void
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->setupWebsite();

        $this->setupStore('English Store', 'main_english_store', 'Scandiweb/default', 'GBP', true);
        $this->setupStore('German Store', 'main_german_store', 'Scandiweb/german');

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    private function setupStore(string $name, string $code, string $theme = null, string $currency = null, bool $default = false): void
    {
        $store = $this->storeFactory->create()
            ->setName($name)
            ->setCode($code)
            ->setWebsiteId(1)
            ->setGroupId(1)
            ->setSortOrder(0)
            ->setIsActive(true);



        $this->storeResource->save($store);

        if ($currency) {
            $this->writer->save('currency/options/default', $currency, 'stores', $store->getId());
            $this->writer->save('currency/options/allow', $currency, 'stores', $store->getId());
        }

        if ($default) {
            $group = $store->getGroup()->setDefaultStoreId($store->getId());
            $this->groupResource->save($group);
        }

        if ($theme) {
            $this->assignTheme($store->getId(), $theme);
        }
    }

    private function setupWebsite(): void
    {
        $this->writer->save('catalog/seo/product_url_suffix', '', 'websites', 1);
        $this->writer->save('catalog/seo/category_url_suffix', '', 'websites', 1);
    }


    /**
     * Assign theme.
     *
     * @param int $storeId
     * @param string $themeName
     */
    protected function assignTheme(int $storeId, string $themeName): void
    {
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();

        foreach ($themes as $theme) {
            if ($theme->getCode() == $themeName) {
                $this->writer->save('design/theme/theme_id', $theme->getId(), 'stores', $storeId);
            }
        }
    }


}