var config = {
    deps: [
        "js/fontawesome",
    ],
    map: {
        '*': {
            slick: 'js/slick',
        },
    },
    shim: {
        'js/slick': ['jquery'],
    },
};
