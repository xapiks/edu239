const fs = require('fs');
const installed = fs.existsSync('index.php');
const bootstrap = require('./' + (installed ? 'dev/tools/' : '')
  + 'gulp/bootstrap'),
  gulp = require('gulp');

gulp.tasks = bootstrap().tasks;
