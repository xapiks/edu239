'use strict';

/**
 * Default includes, that can be re-used in all themes by default
 * @type {string[]}
 */
const defaultIncludes = [
  'bourbon/core',
];

/**
 * Helper function, for extending includes
 *
 * @param dependencies {string[]}
 * @returns {string[]}
 */
function extendInclude(dependencies) {
  if (!dependencies) {
    console.log('You must specify at least one additional include');
    process.exit(1);
  }
  let includes = defaultIncludes;
  includes.push(...dependencies);
  return includes;
}

/**
 * Define Themes
 *
 * area: area, one of (frontend|adminhtml|doc),
 * name: theme name in format Vendor/theme-name,
 * parent:
 *  if defined - add as parent theme
 *  if missing - theme.xml is generated with empty parent
 * files: [
 * 'css/styles-m',
 * 'css/styles-l'
 * ],
 *
 */

/**
 * In order to add bourbon + bourbon-neat + normalize-css as a starting point,
 * main application styles file should import the following directives:
 * @import 'normalize';
 * @import 'bourbon';
 * @import 'neat';
 *
 * @include normalize();
 *
 * @type {string[]}
 */

module.exports = {
    blank: {
        area: 'frontend',
        name: 'Scandiweb/base',
        parent: 'Magento/blank',
        files: [
            'css/styles'
        ],
        scss: {
            options: {
                includePaths: extendInclude(['normalize-scss/sass', 'bourbon-neat/app/assets/stylesheets'])
            }
        },
        browsersync: {
            proxy: "https://magento.local",
            rewriteRules: [
                {
                    match: ".magento.local",
                    replace: ""
                }
            ],
            port: 3001,
            ui: {
                port: 3011
            }
        }
    }
};
