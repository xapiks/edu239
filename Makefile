# Define available commands
.PHONY: build full-rebuild push up recreate down cert pull \
 down-rm-volumes logs flushall exec core-up \
 core-down core-logs forcerecreate

# Variables
current_dir := $(shell pwd)
uid := $(shell id -u)
gid := $(shell id -g)
exec_core = docker-compose -f docker-compose.yml -f docker-compose.local.yml -f docker-compose.core.yml -f docker-compose.ssl.yml
exec_default = docker-compose -f docker-compose.yml -f docker-compose.local.yml -f docker-compose.ssl.yml

# Warning! Do not use soft tabs!
up:
	$(exec_default) up -d
	
forcerecreate:
	$(exec_default) up -d --force-recreate

core-up:
	$(exec_core) -f docker-compose.ssl.yml up -d

build:
	$(exec_default) build

pull:
	$(exec_default) pull

down:
	$(exec_default) down --remove-orphans

core-down:
	$(exec_core) down --remove-orphans

down-rm-volumes:
	$(exec_default) down -v

logs:
	$(exec_default) logs -f --tail 1000 $(filter-out $@,$(MAKECMDGOALS))

core-logs:
	$(exec_core) logs -f --tail 1000 $(filter-out $@,$(MAKECMDGOALS))

full-rebuild:
	$(exec_default) build --pull --no-cache

flushall:
	$(exec_default) exec varnish varnishadm "ban req.url ~ /"
	$(exec_default) exec redis redis-cli FLUSHALL

exec:
	$(exec_default) exec -u user app $(filter-out $@,$(MAKECMDGOALS))

%:
	@true

cert:
	mkdir -p opt/cert
	docker run -it --rm --init \
	-e UID=$(uid) \
	-e GID=$(gid) \
	-w="/cert" \
	--mount type=bind,source=$(current_dir)/deploy/shared/conf/local-ssl,target=/cert_config/ \
	--mount type=bind,source=$(current_dir)/opt/cert,target=/cert \
	--mount type=bind,source=$(current_dir)/scripts/helpers/create_certificates,target=/usr/local/bin/create_certificates \
	alpine:latest create_certificates
