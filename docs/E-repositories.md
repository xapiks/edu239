# Repositories

Magento 2 docker template has an upstream located at: [https://bitbucket.org/scandiweb/docker-magento22-template](https://bitbucket.org/scandiweb/docker-magento22-template)

There are 2 possibilities, to build a setup fully integrating Docker into the project, see 
[Single repository](#Single-repository) section.

If you would like to separate Docker infrastracture for many reasons, ex: code delivery is not controlled by you and 
you must provide ready-to-work code, [Two repositories](#Two-repositories) approach is what you need

## Repository cloning
Clone the template repository:

`git clone git@bitbucket.org:scandiweb/docker-magento22-template.git <projectname>`

Remove remote origin, add project specific instead:

`git remote remove origin`

`git remote add origin <url_to_project_repo>`

**Important note:** Docker repository name must be prefixed with *docker-* when [Two repositories](#Two-repositories)
approach is chosen.

Remove existing git history from the filesystem:
`rm -rf .git`

Add all:
`git add .`

Commit
`git commit -m "Docker initial commit"`

Push
`git push`

## Single repository
Single repository approach is great, when both: sourcecode and deployment are fully under your control. It allows you
 to integrate and keep application together with the environment it is designed to work with. 

### Commiting infrastractural changes
You should use your repository as usually, once you are not pushing to the upstream it is no limitations for your 
creativity and improvements! However, you must keep in mind there are configfiles shared across the environments, eg:
 located under `/deploy/shared` or `docker-compose.yml`. Altering these files for specific environment needs may 
 corrupt other environments stability. For details on files structure, please refer to 
 [Template structure overview](/02-structure-overview.md)
 
### Commiting code changes
Docker is configured to work with the sourcecode stored under `/src` folder. By default, folder contains just 
.gitkeep, everything else is ignored in favour of [Multiple repositories](#Multiple-repositories) config. In order to
 use single repository approach, please update /.gitignore accordingly to your project needs. 
 
*Important note* You shall not create another .gitignore under `/src` folder.

## Two repositories
Multiple repositories allows you to pick legacy code or shared code, meanwhile creating and keeping project-specific 
Docker infrastracture. In order to achieve this, simply open `/src` folder and clone your sourcecode there.

From now on there are 2 roots on your local:
`/` - Docker infrastructure root
`/src` - Webroot (Magento project root)

### Setting up 2-repository Docker infrastruture
#### Docker repository
**Important note:** Docker repository must be prefixed with *docker-*, eg: **docker-projectname**. You might need to 
create it manually in most cases. Once done - please refer to [Repository cloning](#Repository-cloning)


**Git notes:** 
* Each root should contain own `.gitignore`.
* Each root (repository) must be updated seaparately: commit code changes from the Webroot, while Docker changes - 
from the project changes. `git remote -v` will always give you a clue which remote repository you are about to push to.