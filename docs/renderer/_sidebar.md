Docker setup:
*   [How to start](/01-how-to-start.md)
*   [Template structure overview](/02-structure-overview.md)
*   [Available services](/03-services.md)
*   [APP service build](/04-app-service-build.md)
*   [APP service startup](/05-app-service-startup.md)
*   [Running docker infrastructure](/06-running-infrastructure.md)
*   [Docker on indvp](/07-docker-on-indvp.md)
*   [Deployments](/08-deployments.md)
*   -----
Appendix:
*   [A: requirements](/A-requirements.md)
*   [B: CLI-commands](/B-cli-commands.md)
*   [C: Template customization](/C-changing-template.md)
*   [D: Xdebug](/D-xdebug.md)
*   [E: Repositories](/E-Repositories.md)
*   -----
*   [Requirements](/A-requirements.md)
*   [Useful cli commands](/B-cli-commands.md)
*   [xDebug](/D-xdebug.md)
