# Documentation hub {docsify-ignore}

> These docs cover docker setup and usage for local/indvp, and it's deployment

#### Currently under development, so the structure and logic can change before stable release {docsify-ignore}

## Reporting issues

Simply create a new ticket here: <https://scandiweb.atlassian.net/secure/CreateIssue.jspa?pid=32401>

Or ask in [#docker](slack://channel?team=scandiweb&id=C60MXN7AL) channel

## Developing docs {docsify-ignore}

Docs written in Markdown, use any editor as you like

[Docsify](https://docsify.js.org/#/?id=docsify) is used to make local preview, with additional plugins and generate [docs.kube.indvp.com](docs.kube.indvp.com)

To use local preview you need:

*   Setup docsify with `npm install`
*   Execute `npm start` in documentation project
*   Preview the changes on [http://localhost:4000](http://localhost:4000)
