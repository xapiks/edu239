## Prerequisites

For more details on each please see [A-requirements.md](A-requirements.md).

### Docker and Docker-compose

Docker and docker-compose must be installed (latest)

### Networking

Must be connected to `Scandiweb Network` (Scandiweb WiFi AP or VPN), to pull pre-built docker images.

### Composer Authentification  

`COMPOSER_AUTH` must be set in order to fetch sources from Magento 2 repository.
You may use your own credentials, or project specific (**important working on Commerce (Enterprise) version** ).

`export COMPOSER_AUTH='{"http-basic":{"repo.magento.com": {"username": "6f36f70f0cb27b0eab47138c69e081e8", "password": "99cfbc3542d15eb333addb7c2fd11fe2"}}}'`

**It is important to execute `export` in the same terminal window you will run `docker-compose up`**
or add it to the .bashrc or corresponding file, depending on your shell and configuration. 

### Composer json file

`composer.json` must be present in `/src` folder in order to run app container.
You can use sample (Magento 2.2 Community) from `/shared`. just run:
 `cp deploy/shared/samples/magento-composer.json src/composer.json`

## Quick start

1.  Set environment variables on host
2.  `git clone git@bitbucket.org:scandiweb/docker-magento22-template.git myproject`
3.  `composer.json` must be present in `/src`
  You can use sample from `/shared`. just issue
 `cp deploy/shared/samples/magento-composer.json src/composer.json`
4. (for Linux only) Create folder for additional mount points (workaround for Linux):
`mkdir -p src/var src/generated src/pub/static src/static`
 

### Running
1.  Run `docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d`
2.  Check with `docker-compose ps` that all containers `Running`
3.  Open <http://localhost:3000> to check if Magento is started

Reference:
[How to connect to Scandiweb VPN](https://scandiweb.atlassian.net/wiki/spaces/NEP/pages/86540295/How+to+connect+to+Scandiweb+VPN)
[Connecting to Scandiweb-Radius Wi-Fi](https://scandiweb.atlassian.net/wiki/spaces/NEP/pages/144244873/Connecting+to+Scandiweb-Radius+Wi-Fi)
